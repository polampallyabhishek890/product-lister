import React from 'react';
// import { Navigate } from 'react-router-dom';

import Home from './views/Home';

const routes = [
    { path: '/', element: <Home/> },
]

export default routes;