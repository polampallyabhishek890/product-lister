import React, { useState } from 'react';
import { BsSearch } from 'react-icons/bs';
import { AiFillCaretDown } from "react-icons/ai";
import Select, { components } from 'react-select';

import Navbar from '../../components/Navbar';
import MainLayout from './MainLayout';
import { products } from '../../data.json'

export default function Home() {
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [changed, setChanged] = useState(true);

    const productOptions = products.map(product => {
        return {
            value: product.name,
            label: product.name
        }
    });

    const ValueContainer = ({ children, ...props }) => {
        return (
            components.ValueContainer && (
                <components.ValueContainer {...props}>
                    {!!children && (
                        <BsSearch size={16} />
                    )}
                    <div style={{ paddingLeft: 15 }}>
                        {children}
                    </div>
                </components.ValueContainer>
            )
        );
    };

    const DropdownIndicator = props => {
        return (
            <components.DropdownIndicator {...props}>
                <AiFillCaretDown size={16} className="select-icon" />
            </components.DropdownIndicator>
        );
    };

    const IndicatorSeparator = props => {
        return null
    }

    const handleChange = (item) => {
        var result = products.filter(obj => {
            return obj.name === item.value
        })
        setSelectedProduct(result[0])
        setChanged(!changed)
    }

    return (
        <div className="home">
            <Navbar />
            <div className="search-select-product">
                <Select
                    className="select-product"
                    classNamePrefix="select"
                    isSearchable
                    components={{ DropdownIndicator, IndicatorSeparator, ValueContainer }}
                    placeholder="Select a Product"
                    options={productOptions}
                    onChange={(value) => handleChange(value)}
                />
            </div>
            <MainLayout selectedProduct={selectedProduct} setSelectedProduct={setSelectedProduct} changed={changed} />
        </div>
    )
}