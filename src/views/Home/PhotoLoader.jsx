import React from 'react'
import classNames from 'classnames'
import { AiFillDelete } from 'react-icons/ai'
import { BiImageAdd } from 'react-icons/bi'
import { Droppable, Draggable } from 'react-beautiful-dnd'

export default function PhotoLoader({ selectedProduct, setSelectedProduct, loadImage }) {
    const deleteItem = (idx) => {
        const newArray = selectedProduct.products;
        newArray.splice(idx, 1)
        setSelectedProduct({
            ...selectedProduct,
            products: [...newArray]
        })
    }

    return (
        <div className="photo-loader">
            {selectedProduct !== null ? (
                <Droppable key="image-picker" droppableId={"image-picker"} type="image-container">
                    {(provided, snapshot) => {
                        return (<div className={classNames("product-grid", { "drag-over": snapshot.isDraggingOver })} {...provided.droppableProps} ref={provided.innerRef}>
                            {selectedProduct.products.map((product, idx) => (
                                <Draggable key={`a-${idx}`} draggableId={`a-${idx}`} index={idx}>
                                    {(provided, snapshot) => {
                                        return (
                                            <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} className={classNames("image-container", { "drag-on": snapshot.isDragging })}>
                                                <img src={product} alt="Image" key={idx} />
                                                <AiFillDelete size={16} className="delete-icon" onClick={() => deleteItem(idx)} />
                                                <BiImageAdd size={16} className="add-icon" onClick={() => loadImage(idx)} />
                                            </div>
                                        )
                                    }}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                        )
                    }}
                </Droppable>
            ) : (
                <div></div>
            )}
        </div>
    )
}
