import React, { useEffect, useState } from 'react'
import { AiOutlineMinusCircle } from 'react-icons/ai'
import { DragDropContext } from 'react-beautiful-dnd'
import {
    ReflexContainer,
    ReflexSplitter,
    ReflexElement
} from 'react-reflex'

import 'react-reflex/styles.css'
import ProductDetails from './ProductDetails'
import PhotoLoader from './PhotoLoader'
import Editor from './Editor'

import { leftPanelMaxSize, leftPanelMinSize, middlePanelMaxSize, middlePanelMinSize, rightPanelMinSize } from '../../config.json'

export default function MainLayout({ selectedProduct, setSelectedProduct, changed }) {
    const [showColumns, setShowColumns] = useState({
        details: true,
        loader: true
    })
    const [imageList, setImageList] = useState([])
    const [imageRequirements, setImageRequirements] = useState({
        1: [
            { displayName: 'Front View', required: 1 },
            { displayName: 'Back View 2', required: 1 },
            { displayName: 'Tag', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
        ],
        2: [
            { displayName: 'Front View', required: 1 },
            { displayName: 'Back View 2', required: 1 },
            { displayName: 'Side View', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
        ],
        3: [
            { displayName: 'Front View', required: 1 },
            { displayName: 'Back View 2', required: 1 },
            { displayName: 'Buckle View', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
        ],
        4: [
            { displayName: 'Front View Full', required: 1 },
            { displayName: 'Back View 2 Full', required: 1 },
            { displayName: 'Pattern View', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
        ],
        5: [
            { displayName: 'Front View', required: 1 },
            { displayName: 'Back View 2', required: 1 },
            { displayName: 'Pattern View', required: 1 },
            { displayName: 'Main Tag', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
            { displayName: 'Optional Tag', required: 0 },
        ],
        6: [
            { displayName: 'Full Head View', required: 1 },
            { displayName: 'Front Forehead View', required: 1 },
            { displayName: 'Top View', required: 1 },
            { displayName: 'Back View', required: 1 },
            { displayName: 'Pattern View', required: 0 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
        ],
        7: [
            { displayName: 'Top View', required: 1 },
            { displayName: 'Bottom View', required: 1 },
            { displayName: 'Side View', required: 1 },
            { displayName: 'Main Tag', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
        ],
        8: [
            { displayName: 'Front View', required: 1 },
            { displayName: 'Back View 2', required: 1 },
            { displayName: 'Pattern View', required: 1 },
            { displayName: 'Main Tag', required: 1 },
            { displayName: 'Optional View 1', required: 0 },
            { displayName: 'Optional View 2', required: 0 },
            { displayName: 'Optional View 3', required: 0 },
            { displayName: 'Optional Tag', required: 0 },
        ],
    })

    useEffect(() => {
        if (selectedProduct) {
            setImageArray()
        }
    }, [changed])

    const setImageArray = () => {
        const typeId = selectedProduct.productTypeId;
        let array = new Array();
        for (let i = 0; i < imageRequirements[typeId].length; i++) {
            let image = {
                name: imageRequirements[typeId][i].displayName,
                required: imageRequirements[typeId][i].required
            }
            array.push(image)
        }
        setImageList([...array]);
    }

    const changeName = (index, value) => {
        let newList = imageList;
        newList[index].name = value;
        setImageList([...newList]);
    }

    const deleteImage = (index) => {
        let array = imageList;
        const typeId = selectedProduct.productTypeId;

        if (array.length > imageRequirements[typeId].length) {
            array.pop()
            setImageList([...array])
        } else {
            delete array[index].image
            setImageList([...array]);
        }
    }

    const loadImage = (id) => {
        let array = imageList;
        const typeId = selectedProduct.productTypeId;
        let index = array.findIndex(el => el.image === undefined)
        console.log(index, imageRequirements[typeId].length)

        if (!array[index]) {
            let image = {
                name: `Image ${array.length + 1}`,
                image: selectedProduct.products[id],
                required: 0
            }
            array.push(image)
            setImageList([...array])
        } else {
            array[index].image = selectedProduct.products[id]
            setImageList([...array])
        }
    }

    const onDragEnd = (result) => {
        if (!result.destination) return;
        const { source, destination } = result;

        if (source.droppableId === "image-list" && destination.droppableId === "image-list") {
            const array = imageList
            if (array[source.index] && array[destination.index]) {
                array.splice(destination.index, 0, array.splice(source.index, 1)[0])
            }
            setImageList([...array])
        }

        if (source.droppableId === "image-picker" && destination.droppableId === "image-picker") {
            const newArray = selectedProduct.products
            newArray.splice(destination.index, 0, newArray.splice(source.index, 1)[0]);

            setSelectedProduct({
                ...selectedProduct,
                products: [...newArray]
            })
        }
    }

    const addCroppedImage = (image, index) => {
        let array = imageList

        array[index].image = image
        setImageList([...array])

        // let productArray = selectedProduct.products
        // productArray.push(image)
        // setSelectedProduct({
        //     ...selectedProduct,
        //     products: productArray
        // })
    }

    const undoCrop = (image, index) => {
        let array = imageList

        array[index].image = image
        setImageList([...array])
    }

    console.log(imageList);

    return (
        <div className="main-layout">
            <DragDropContext onDragEnd={result => onDragEnd(result)}>
                <ReflexContainer orientation="vertical">
                    <ReflexElement className="left-pane" size={!showColumns.details && 0} minSize={showColumns.details ? leftPanelMinSize : 0} maxSize={leftPanelMaxSize}>
                        <div className="container">
                            <div className="header">
                                <h6>Details</h6>
                                <AiOutlineMinusCircle size={16} onClick={() => setShowColumns({ ...showColumns, details: false })} />
                            </div>
                            <ProductDetails selectedProduct={selectedProduct} />
                        </div>
                    </ReflexElement>
                    <ReflexSplitter className="resizer" onResize={() => setShowColumns({ ...showColumns, details: true })} />
                    <ReflexElement className="middle-pane" size={!showColumns.loader && 0} minSize={showColumns.loader ? middlePanelMinSize : 0} maxSize={middlePanelMaxSize}>
                        <div className="container">
                            <div className="header">
                                <h6>Photo Loader</h6>
                                <AiOutlineMinusCircle size={16} onClick={() => setShowColumns({ ...showColumns, loader: false })} />
                            </div>
                            <PhotoLoader selectedProduct={selectedProduct} setSelectedProduct={setSelectedProduct} loadImage={loadImage} />
                        </div>
                    </ReflexElement>
                    <ReflexSplitter propagate={true} className="resizer" onResize={() => setShowColumns({ ...showColumns, loader: true })} />
                    <ReflexElement className="right-pane" minSize={rightPanelMinSize}>
                        <div className="container image-editor-section">
                            <Editor imageList={imageList} changeName={changeName} deleteImage={deleteImage} addCroppedImage={addCroppedImage} undoCrop={undoCrop} />
                        </div>
                    </ReflexElement>
                </ReflexContainer>
            </DragDropContext>
        </div>
    )
}
