import React, { useState, useEffect } from 'react'

import { productTypeAttributes } from '../../../data.json'
import ComboBox from './ComboBox'
import NumberInput from './NumberInput'
import Checkbox from './Checkbox'
import LineEdit from './LineEdit'

export default function ItemDetails({ selectedProduct }) {
    const [values, setValues] = useState({});
    const [submitValues, setSubmitValues] = useState(false);
    const [errorText, setErrorText] = useState('')
    const [typeId, setTypeId] = useState(null)
    const [filteredResults, setFilteredResults] = useState([])

    useEffect(() => {
        setFilteredResults(productTypeAttributes.filter(option => option.ProductTypeID === typeId))
    }, [typeId])

    useEffect(() => {
        if (selectedProduct) {
            setTypeId(selectedProduct.productTypeId)
        }
    }, [selectedProduct])

    const handleChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        })
    }

    const handleSubmit = () => {
        setErrorText('')
        console.log(values)
    }

    const validateFields = () => {
        const requiredFields = productTypeAttributes.filter(option => option.InputRequired === 1).map(option => option.Attribute)
        const tempValues = Object.keys(values)
            .filter(key => {
                if (requiredFields.includes(key)) {
                    return true
                } else {
                    return false
                }
            })
            .reduce((obj, key) => {
                obj[key] = values[key];
                return obj;
            }, {});

        setSubmitValues(tempValues);

        const valueArray = Object.values(tempValues).map(value => value);

        return (Object.keys(valueArray).length !== requiredFields.length || valueArray.some(el => el === ""))
    }

    if (typeId) {
        return (
            <div className="item-details">
                {filteredResults.map((product, idx) => (<>
                    {product.InputMethod === "combo_box" && (
                        <ComboBox title={product.Attribute} required={product.InputRequired === 1} handleChange={handleChange} submitValues={submitValues} />
                    )}
                    {product.InputMethod === "numeric" && (
                        <NumberInput title={product.Attribute} required={product.InputRequired === 1} handleChange={handleChange} submitValues={submitValues} />
                    )}
                    {product.InputMethod === "boolean" && (
                        <Checkbox title={product.Attribute} required={product.InputRequired === 1} handleChange={handleChange} submitValues={submitValues} />
                    )}
                    {product.InputMethod === "line_edit" && (
                        <LineEdit title={product.Attribute} required={product.InputRequired === 1} handleChange={handleChange} submitValues={submitValues} />
                    )}
                </>))}
                <button onClick={() => {
                    {
                        validateFields() ? setErrorText('Please fill all the required values') : handleSubmit()
                    }
                }}>Get values</button>
                {errorText.length > 0 && (
                    <p className="error-message">{errorText}*</p>
                )}
            </div>
        )
    } else {
        return (
            <div></div>
        )
    }
}
