import React, { useState, useEffect } from 'react'
import classNames from 'classnames'

export default function ComboBox({ title, required = true, handleChange, submitValues, selectOptions = false }) {
    const [value, setValue] = useState('');
    const [error, setError] = useState('');
    const options = selectOptions ? [...selectOptions] : ['Option 1', 'Option 2', 'Option 3', 'Option 4'];
    const [filteredOptions, setFilteredOptions] = useState([])

    useEffect(() => {
        if (value !== '') {
            setFilteredOptions(options.filter(option => option && option.toString().toLowerCase().search(value.toLowerCase()) !== -1))
            setError('')
        } else {
            setFilteredOptions([])
        }
    }, [value])

    useEffect(() => {
        if (required && submitValues) {
            if (!submitValues[title] || submitValues[title] === "") {
                setError(`${title} is required`)
            } else {
                setError('')
            }
        }
    }, [submitValues])

    return (
        <div className="combo-box">
            <p className="title">{title} {required && '*'}</p>
            <input type="text" className={classNames({ error: error !== '' })} name={title} value={value} onChange={e => {
                setValue(e.target.value)
                handleChange(e)
            }} />
            {error !== '' && (
                <p className="error-text">{error}</p>
            )}
            {filteredOptions.length > 0 && (
                <div className="options">
                    {filteredOptions.map((option, idx) => (
                        <div className="option" key={idx} onClick={() => {
                            setValue(option)
                            setFilteredOptions([])
                        }}>{option}</div>
                    ))}
                </div>
            )}
            {filteredOptions.length > 0 && (
                <div className="transparent-overlay" onClick={() => setFilteredOptions([])}></div>
            )}
        </div>
    )
}
