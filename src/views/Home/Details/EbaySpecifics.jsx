import React, { useState, useEffect } from 'react'

import ComboBox from './ComboBox'
import SelectBox from './SelectBox'
import EbaySmartSearch from './EbaySmartSearch'
import { ebayAttributes, ebayAttributeRecommendations } from '../../../data.json'

export default function EbaySpecifics() {
    const [values, setValues] = useState({});
    const [submitValues, setSubmitValues] = useState(false);
    const [errorText, setErrorText] = useState('');
    const options = ebayAttributes;
    const [filteredOptions, setFilteredOptions] = useState([]);
    const [categoryId, setCategoryId] = useState(null);

    useEffect(() => {
        setFilteredOptions(options.filter(option => option.EbayCategoryID === categoryId))
    }, [categoryId])

    const handleChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        });
    }

    const handleSubmit = () => {
        setErrorText('');
        console.log(submitValues);
    }

    const validateType = () => {
        const requiredFields = options.filter(option => option.UsageConstraint === "Required").map(option => option.EbayAttributeName)
        const tempValues = Object.keys(values)
            .filter(key => {
                if (requiredFields.includes(key)) {
                    return true
                } else {
                    return false
                }
            })
            .reduce((obj, key) => {
                obj[key] = values[key];
                return obj;
            }, {});

        setSubmitValues(tempValues);

        const valueArray = Object.values(tempValues).map(value => value);

        return (Object.keys(valueArray).length !== requiredFields.length || valueArray.some(el => el === ""))
    }

    return (
        <div className="ebay-details">
            <EbaySmartSearch handleChange={setCategoryId} />
            {filteredOptions.map((option, idx) => (
                <>
                    {option.SelectionMode === "FreeText" && (
                        <ComboBox
                            title={option.EbayAttributeName}
                            required={option.UsageConstraint === "Required"}
                            handleChange={handleChange}
                            submitValues={submitValues}
                            selectOptions={Object.values(ebayAttributeRecommendations).filter(attribute => attribute.EbayAttributeID === option.EbayAttributeID).map(attribute => attribute.RecommendedValue)}
                        />
                    )}
                    {option.SelectionMode === "SelectionOnly" && (
                        <SelectBox
                            title={option.EbayAttributeName}
                            required={option.UsageConstraint === "Required"}
                            handleChange={handleChange}
                            submitValues={submitValues}
                            selectOptions={Object.values(ebayAttributeRecommendations).filter(attribute => attribute.EbayAttributeID === option.EbayAttributeID).map(attribute => attribute.RecommendedValue)}
                        />
                    )}
                </>
            ))}
            <button onClick={() => {
                {
                    validateType() ? setErrorText('Please fill all the required values') : handleSubmit()
                }
            }}>Get values</button>
            {errorText.length > 0 && (
                <p className="error-message">{errorText}</p>
            )}
        </div>
    )
}
