import React, { useState, useEffect } from 'react'
import classNames from 'classnames'

export default function NumberInput({ title, required = true, handleChange, submitValues }) {
    const [value, setValue] = useState('');
    const [error, setError] = useState('')

    useEffect(() => {
        if (required && submitValues) {
            if (!submitValues[title] || submitValues[title] === "") {
                setError(`${title} is required`)
            } else {
                setError('')
            }
        }
    }, [submitValues])

    return (
        <div className="combo-box">
            <p className="title">{title} {required && '*'}</p>
            <input type="number" className={classNames({ error: error !== '' })} name={title} value={value} min={0} maxLength={10} onChange={e => {
                setValue(e.target.value)
                handleChange(e)
                if (e.target.value !== '') {
                    setError('')
                }
            }} />
            {error !== '' && (
                <p className="error-text">{error}</p>
            )}
        </div>
    )
}
