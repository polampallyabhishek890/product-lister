import React, { useState, useEffect } from 'react'
import classNames from 'classnames'

export default function ComboBox({ title, required = true, handleChange, submitValues, selectOptions = false }) {
    const [searchValue, setSearchValue] = useState('')
    const [value, setValue] = useState('');
    const options = selectOptions ? [...selectOptions] : ['Option 1', 'Option 2', 'Option 3', 'Option 4'];
    const [filteredOptions, setFilteredOptions] = useState([])
    const [showOptions, setShowOptions] = useState(false)
    const [error, setError] = useState('')

    useEffect(() => {
        setFilteredOptions(options.filter(option => option && option.toString().toLowerCase().search(searchValue.toLowerCase()) !== -1))
    }, [searchValue])

    useEffect(() => {
        const e = {
            target: {
                name: title,
                value: value
            }
        }
        handleChange(e)
        setError('')
    }, [value])

    useEffect(() => {
        if (required && submitValues) {
            if (!submitValues[title] || submitValues[title] === "") {
                setError(`${title} is required`)
            } else {
                setError('')
            }
        }
    }, [submitValues])

    return (
        <div className="combo-box">
            <p className="title">{title} {required && '*'}</p>
            <div className={classNames("input", { error: error !== '' })} onClick={() => setShowOptions(true)}>
                <input type="text" value={value} disabled placeholder="Select an option" />
            </div>
            {error !== '' && (
                <p className="error-text">{error}</p>
            )}
            {showOptions && (
                <div className="options">
                    <div className="search-input">
                        <input type="text" value={searchValue} placeholder="Search..." onChange={e => {
                            setSearchValue(e.target.value)
                        }} />
                    </div>
                    {filteredOptions.length > 0 ? filteredOptions.map((option, idx) => (
                        <div className="option" key={idx} onClick={() => {
                            setValue(option)
                            setShowOptions(false)
                        }}>{option}</div>
                    )) : (
                        <div className="empty-text">
                            <p>No options. Please try something else</p>
                        </div>
                    )}
                </div>
            )}
            {showOptions && (
                <div className="transparent-overlay" onClick={() => setShowOptions(false)}></div>
            )}
        </div>
    )
}
