import React, { useState, useEffect } from 'react'

export default function Checkbox({ title, required = true, handleChange }) {
    const [checked, setChecked] = useState(false)

    useEffect(() => {
        const e = {
            target: {
                name: title,
                value: checked
            }
        }
        handleChange(e)
    }, [])

    useEffect(() => {
        const e = {
            target: {
                name: title,
                value: checked
            }
        }
        handleChange(e)
    }, [checked])

    return (
        <div class="checkbox">
            <p className="title">{title} {required && '*'}</p>
            <input class="switch" type="checkbox" checked={checked} onChange={(e) => setChecked(!checked)} />
        </div>
    )
}
