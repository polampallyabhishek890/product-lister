import React, { useState, useEffect } from 'react'
import classNames from 'classnames'

import { ebayCategories } from '../../../data.json'

export default function EbaySmartSearch({ handleChange }) {
    const [searchValue, setSearchValue] = useState('');
    const [value, setValue] = useState(null);
    const options = [...ebayCategories];
    const [filteredOptions, setFilteredOptions] = useState([])
    const [showOptions, setShowOptions] = useState(false)

    useEffect(() => {
        setFilteredOptions(Object.values(options).filter(option => ((option.CategoryID.toString().search(searchValue.toLowerCase()) !== -1) || (option.CategoryPath.toLowerCase().search(searchValue.toLowerCase()) !== -1))))
    }, [searchValue])

    useEffect(() => {
        if (value) {
            handleChange(value.CategoryID)
        }
    }, [value])

    return (
        <div className="combo-box">
            <p className="title">Select Ebay Category</p>
            <div className={classNames("input")} onClick={() => setShowOptions(true)}>
                <input type="text" value={value ? value.BaseCategory + ' > ' + value.LastCategory : null} disabled placeholder="Select an option" />
            </div>
            {showOptions && (
                <div className="options">
                    <div className="search-input">
                        <input type="text" value={searchValue} placeholder="Search..." onChange={e => {
                            setSearchValue(e.target.value)
                        }} />
                    </div>
                    {filteredOptions.length > 0 ? filteredOptions.map((option, idx) => (
                        <div className="option" key={idx} onClick={() => {
                            setValue(option)
                            setShowOptions(false)
                        }}>{option.CategoryID} - {option.CategoryPath}</div>
                    )) : (
                        <div className="empty-text">
                            <p>No options. Please try something else</p>
                        </div>
                    )}
                </div>
            )}
            {showOptions && (
                <div className="transparent-overlay" onClick={() => setShowOptions(false)}></div>
            )}
        </div>
    )
}
