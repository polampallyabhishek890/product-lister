import React, { useEffect, useState } from 'react'
import { Droppable } from 'react-beautiful-dnd'

// import { defaultImageUploadCount } from '../../../config.json'
import ImageHolder from './ImageHolder'

export default function PlaceImages({ imageList, changeName, deleteImage, selectedImage, setSelectedImage, setSelectedIndex }) {
    const [collapsedItems, setCollapsedItems] = useState([]);

    console.log(collapsedItems);

    useEffect(() => {
        if (imageList.length === 0) {
            setSelectedImage(null)
        }
    }, [imageList])

    return (
        <Droppable key="image-list" droppableId={"image-list"} direction="horizontal" type="image-placement">
            {(provided, snapshot) => {
                return (
                    <div className="images-list" {...provided.droppableProps} ref={provided.innerRef}>
                        {imageList?.map((image, idx) => (
                            <ImageHolder
                                image={image}
                                changeName={changeName}
                                index={idx}
                                deleteImage={deleteImage}
                                selectedImage={selectedImage}
                                setSelectedImage={setSelectedImage}
                                setSelectedIndex={setSelectedIndex}
                                collapsedItems={collapsedItems}
                                setCollapsedItems={setCollapsedItems}
                            />
                        ))}
                        {provided.placeholder}
                    </div>
                )
            }}
        </Droppable>
    )
}
