import React, { useState } from 'react'
import classNames from 'classnames'
import PlaceImages from './PlaceImages';
import ImageEditor from './ImageEditor';

export default function Editor({ imageList, changeName, deleteImage, addCroppedImage, undoCrop }) {
    const [selectedImage, setSelectedImage] = useState(null);
    const [selectedIndex, setSelectedIndex] = useState(null);

    return (
        <div className="editor">
            <div className="image-placement">
                <div className="header">
                    <h6>Image Placement</h6>
                </div>
                <div className={classNames("images-container")}>
                    <PlaceImages imageList={imageList} changeName={changeName} deleteImage={deleteImage} selectedImage={selectedImage} setSelectedImage={setSelectedImage} setSelectedIndex={setSelectedIndex} />
                </div>
            </div>
            <div className={classNames("image-editor")}>
                <div className="header">
                    <h6>Image Editor</h6>
                </div>
                <ImageEditor selectedImage={selectedImage} selectedIndex={selectedIndex} addCroppedImage={addCroppedImage} undoCrop={undoCrop} />
            </div>
        </div>
    )
}
