import React, { useRef, useState, useEffect } from 'react'
import classNames from 'classnames'
import fs from 'browserify-fs'
import { GrRevert } from 'react-icons/gr'
import { MdSend } from 'react-icons/md'
import { BiCrop } from 'react-icons/bi'
import { FiMove } from 'react-icons/fi'
import Cropper from "react-cropper"
import "cropperjs/dist/cropper.css"

import { initialAspectRatio, zoomDelta } from '../../../config.json'

export default function ImageEditor({ selectedImage, addCroppedImage, selectedIndex, undoCrop }) {
    const [zoom, setZoom] = useState(0.35);
    const [imageCrop, setImageCrop] = useState(true);
    const cropperRef = useRef(null);
    const [cropped, setCropped] = useState(false);
    const [dimensions, setDimensions] = useState({
        width: '',
        height: ''
    })
    const [imageData, setImageData] = useState(null);
    const imageBackup = selectedImage;

    useEffect(() => {
        setZoom(0.35);
        setImageData(null);
    }, [selectedImage])

    const onCrop = (e) => {
        const imageElement = cropperRef?.current;
        const cropper = imageElement?.cropper;
        setImageData(cropper.getCroppedCanvas().toDataURL())

        setDimensions({
            width: Math.ceil(e.detail.width),
            height: Math.ceil(e.detail.height)
        })
    };

    const calculateZoom = () => {
        return Math.floor(zoom / 0.35 * 100);
    }

    const exportImage = () => {
        addCroppedImage(imageData, selectedIndex);
        setCropped(true);
        const imageElement = cropperRef?.current;
        const cropper = imageElement?.cropper;
        cropper.replace(imageData)
    }

    return (
        <div className="photo-editor">
            {selectedImage !== null && (
                <>
                    <div className="crop-container">
                        <Cropper
                            src={selectedImage}
                            checkCrossOrigin={false}
                            style={{ height: "350px", width: "150px" }}
                            // Cropper.js options
                            initialAspectRatio={initialAspectRatio}
                            guides={true}
                            crop={onCrop}
                            ref={cropperRef}
                            zoomTo={zoom}
                            center={true}
                            modal={false}
                            background={false}
                            viewMode={3}
                        />
                    </div>
                    <div className="cropper-options">
                        <GrRevert size={24} onClick={() => {
                            undoCrop(imageBackup, selectedIndex)
                            const imageElement = cropperRef?.current;
                            const cropper = imageElement?.cropper;
                            cropper.replace(imageBackup)
                        }} />
                        <div className="crop-modal">
                            <div className="dimensions">
                                <p>{dimensions.width}px</p>
                                <p>by</p>
                                <p>{dimensions.height}px</p>
                            </div>
                            <div className="zoom-box">
                                <p className="zoomer" onClick={() => setZoom(zoom - zoomDelta)}>-</p>
                                <p>{calculateZoom()}%</p>
                                <p className="zoomer" onClick={() => setZoom(zoom + zoomDelta)}>+</p>
                            </div>
                            <div className="crop-move">
                                <BiCrop size={22} className={classNames({ active: imageCrop })} onClick={() => setImageCrop(true)} />
                                <FiMove size={22} className={classNames({ active: !imageCrop })} onClick={() => setImageCrop(false)} />
                            </div>
                        </div>
                        <MdSend size={24} onClick={() => exportImage()} />
                    </div>
                </>
            )}
        </div>
    )
}
