import React, { useState, useEffect } from 'react'
import classNames from 'classnames'
import { BsCardImage } from 'react-icons/bs'
import { AiOutlineEdit, AiOutlineCheck, AiOutlineDelete } from 'react-icons/ai'
import { CgArrowsExpandDownLeft } from 'react-icons/cg'
import { IoIosArrowForward } from 'react-icons/io'
import { Draggable } from 'react-beautiful-dnd'

export default function ImageHolder({
    image,
    changeName,
    index,
    deleteImage,
    selectedImage,
    setSelectedImage,
    setSelectedIndex,
    collapsedItems,
    setCollapsedItems
}) {
    const [editing, setEditing] = useState(false);
    const [value, setValue] = useState('');
    const [collapse, setCollapse] = useState(false);

    useEffect(() => {
        if (editing) {
            setValue(image.name)
        }
    }, [editing])

    const handleChange = (e) => {
        e.preventDefault();
        setValue(e.target.value);
    }

    const handleEnter = (e) => {
        if (e.keyCode === 13) {
            setEditing(false)
            applyChanges();
        }
    }

    const applyChanges = () => {
        changeName(index, value);
        setValue('');
    }

    useEffect(() => {
        if (collapsedItems.includes(image.name)) {
            setSelectedImage(null)
            setCollapse(true)
        } else {
            setCollapse(false)
        }
    }, [collapsedItems])

    return (
        <Draggable key={`b-${index}`} draggableId={`b-${index}`} index={index} isDragDisabled={collapse}>
            {(provided, snapshot) => {
                return (
                    <div
                        className={classNames("image-holder", { selected: selectedImage === image.image }, { shrink: collapse })}
                        onClick={() => {
                            if (collapse) {
                                console.log('object')
                                let items = collapsedItems
                                let index = collapsedItems.findIndex(el => el === image.name)
                                items.splice(index, 1)
                                setCollapsedItems([...items])
                            }
                        }}
                        key={index}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}>
                        <div className="input">
                            <input type="text" value={editing ? value : image.name} disabled={!editing} onChange={handleChange} onKeyDown={handleEnter} />
                            {image.required ? (
                                <p className="required">*</p>
                            ) : (
                                <p></p>
                            )}
                            {editing ? (
                                <AiOutlineCheck size={16} className="edit-icon" onClick={() => {
                                    setEditing(false)
                                    applyChanges()
                                }} />
                            ) : (
                                <AiOutlineEdit size={16} className="edit-icon" onClick={() => setEditing(true)} />
                            )}
                        </div>
                        {collapse && <IoIosArrowForward size={16} className="expand-icon" />}
                        {image.image ? (
                            <>
                                <CgArrowsExpandDownLeft size={14} className="collapse-icon" onClick={() => {
                                    let array = collapsedItems
                                    array.push(image.name)
                                    setCollapsedItems([...array])
                                }} />
                                <AiOutlineDelete size={16} className="delete-icon" onClick={() => deleteImage(index)} />
                                <div className="image-zone selected-image" onClick={() => {
                                    if (image.image && !collapse) {
                                        setSelectedImage(image.image)
                                        setSelectedIndex(index)
                                    }
                                }}>
                                    <img src={image.image} alt="Image" />
                                </div>
                            </>
                        ) : (
                            <div className={classNames("image-zone", { require: image.required === 1, "not-require": image.required === 0 })}>
                                <BsCardImage size={16} />
                            </div>
                        )}
                    </div>
                )
            }}
        </Draggable>
    )
}
