import React, { useState } from 'react'
import classNames from 'classnames'
import { AiFillCaretDown, AiFillCaretUp } from 'react-icons/ai'

import ItemDetails from './Details/ItemDetails'
import EbaySpecifics from './Details/EbaySpecifics'
import PoshmarkSpecifics from './Details/PoshmarkSpecifics'
import MercariSpecifics from './Details/MercariSpecifics'

export default function ProductDetails({ selectedProduct }) {
    const [selectedSection, setSelectedSection] = useState(0)
    const sections = ['Item Details', 'Poshmark Category Specifics', 'Mercari Category Specifics', 'Ebay Category Specifics']
    return (
        <div className="details-bar">
            {sections.map((section, idx) => (
                <div className="section">
                    <div className="section-bar" onClick={() => {
                        if (selectedSection === idx) {
                            setSelectedSection(null)
                        } else {
                            setSelectedSection(idx)
                        }
                    }}>
                        <p>{section}</p>
                        {selectedSection === idx ? (
                            <AiFillCaretUp size={14} />
                        ) : (
                            <AiFillCaretDown size={14} />
                        )}
                    </div>
                    <div className={classNames("section-info", { expanded: selectedSection === idx })}>
                        {selectedSection === 0 && idx === 0 && (
                            <ItemDetails selectedProduct={selectedProduct} />
                        )}
                        {selectedSection === 1 && idx === 1 && (
                            <PoshmarkSpecifics />
                        )}
                        {selectedSection === 2 && idx === 2 && (
                            <MercariSpecifics />
                        )}
                        {selectedSection === 3 && idx === 3 && (
                            <EbaySpecifics selectedProduct={selectedProduct} />
                        )}
                    </div>
                </div>
            ))}
        </div>
    )
}
