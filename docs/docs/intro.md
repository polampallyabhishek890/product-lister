---
sidebar_position: 1
---

# Getting Started

If you are already familiar with Github and have a Git account, just go to the link below and clone the repository in
any editor(VS Code). If not then follow these steps.

## Clone the Git Repository

Just follow the instructions given in this link to setup your Git account on your Computer.
[Setup Git](https://docs.github.com/en/get-started/quickstart)

Once you are done with that, open your computer terminal and run this command.

```shell
git clone https://gitlab.com/polampallyabhishek890/product-lister.git app-name
```

## Install the dependencies

Before you can run the application, you need to have the dependencies installed. Just go to your project folder in any terminal
and run this command

```shell
npm install
```

## Start your site

Run the development server:

```shell
npm start
```

Your site starts at `http://localhost:3000`.
