# Details

This section is about the left most section, where users can add information about the product that is selected. I am guessing most of it will be coming from the backend. 

If you open ItemDetails.jsx file in Home/Details folder, you will find a SelectedProduct Prop which will contain the currently selected item and a useEffect Hook which will set the TypeId of the component everytime it changes. 

All the inputs are being rendered through the data which is coming from the data.json file. It is taken from the csv file, so if you wish to add some fields, I suggest you add it there and convert it to JSON format and adding it to the data.json file. 


## Adding Poshmark or Mercari Specfics

For adding these forms, you need the json data from a csv file. Everything else if the same as the Ebay Specifics file, the code detects the input type and categoryId and renders a different form field. 

Please check the naming before implementing this, since the Item details and the EBay Specifics had different naming structure for their data.

## Output Value

You can find the output of the ItemDetails or EBaySpecifics section in the browser console. In the code, a variable named submitValues holds the values that the user has selected or typed.