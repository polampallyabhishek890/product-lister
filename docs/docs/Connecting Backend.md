# Connecting the Backend

All the functions related to the saving data and fetching data is inside the MainLayout.jsx file. Please refer the code inside that, if you are connecting the backend. 

The cropper image is currently in binary format, so depending on the backend the image needs to be converted before sending it. 

## State Management

Currently I have not implemented any state management for this application, because the data was static. Once you have the backend connected, you need to implement some Global State Management solution, to cache and limit the requests to the backend server.

