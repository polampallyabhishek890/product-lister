# Structure of the code

I have made this section to simplify the code for other developers, so it is easier for them to understand and change the code.

1. All the code will be present inside the **src** folder

2. You can find the product information in the data.json file, this is where I am pulling all the data from. You can change the **Product Type Id** or adding new products. 

3. The variables.scss in scss/abstracts folder will contain the configuration variables for the styles of the Application.

4. The React files for this application can be found in views/Home inside the src folder.

5. The Details and Editor sections contain the components for the Details and Image Cropper section respectively.

6. The config.json file contains all the configuration variables for the App functionality.

7. This application follow scss styling standards and each component and page in the application has a different stylesheet. So if you need to change the styles, it is very convinient to do so. 