# Image Cropper

For the Image Cropper, I am using a library called cropper-js. You can find the docs about it online, I have added all the required features but if you want to change something or add any more features to the application -> [Here is the link](https://github.com/fengyuanchen/cropperjs)

## Image Data

The data about a particular image(required or not, display name) has been given in the MainLayout.jsx. You can change the names and required status over there. Once you connect the backend, you can set the data to the Image Requirements array in a similar format.

The output of the data is stored in a variable called ImageData which will be in the MainLayout.jsx file. The output is console logged for every change.